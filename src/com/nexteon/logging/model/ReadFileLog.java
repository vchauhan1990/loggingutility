package com.nexteon.logging.model;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.nexteon.logging.api.AnalyticsEntityService;
import com.nexteon.logging.api.MaxMindService;
import com.nexteon.logging.api.NetworksStatsService;
import com.nexteon.logging.impl.AnalyticsEntityServiceImpl;
import com.nexteon.logging.impl.MaxMindServiceImpl;
import com.nexteon.logging.impl.NetworkStatsServiceImpl;

/**
 * Created by admin on 11-May-17.
 */
public class ReadFileLog extends Thread{

    AnalyticsEntityService analyticsEntityService = new AnalyticsEntityServiceImpl();
    
    MaxMindService maxMindService = new MaxMindServiceImpl();

    NetworksStatsService networksStatsService = new NetworkStatsServiceImpl();
    
    private File logfile = new File("C:/Program Files (x86)/Wowza Media Systems/Wowza Streaming Engine 4.7.0/logs/wowzastreamingengine_access.log");
    private boolean startAtBeginning = false;
    private boolean tailing = false;

    public void run()
    {
    	System.out.println("Starting app to send data...............................");
        long filePointer = 0;
        if( this.startAtBeginning )
        {
            filePointer = 0;
        }
        else
        {
            filePointer = this.logfile.length();
        }

        try
        {
            this.tailing = true;
            RandomAccessFile file = new RandomAccessFile( logfile, "r" );
            while( this.tailing )
            {
                try
                {
                    long fileLength = this.logfile.length();
                    if( fileLength < filePointer )
                    {
                        file = new RandomAccessFile( logfile, "r" );
                        filePointer = 0;
                    }

                    if( fileLength > filePointer )
                    {
                        file.seek( filePointer );
                        String line = file.readLine();
                        while( line != null )
                        {
                            makeDBEntry(line);
                            line = file.readLine();
                            
                        }
                        filePointer = file.getFilePointer();
                    }
                }
                catch( Exception e )
                {
                }
                Thread.sleep(2000);
            }
            file.close();
            System.out.println("Stooping app to send data...............................");
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

    private void makeDBEntry(String line) {
        System.out.println(line);
        String[] logData = line.split("\t");
        AnalyticsEntity analyticsEntity = new AnalyticsEntity(logData);
        if(!(analyticsEntity.getCsUriStem().equals("-") || analyticsEntity.getCsUriStem().equals("null"))) {
        	if(analyticsEntity.getcIP()!= null && !analyticsEntity.getcIP().equals("") && analyticsEntity.getxEvent().equals("play")){
            	System.out.println("ABOUT TO ENTER GEOIP");
            	MaxMindData objmaxMindData = maxMindService.getCityData(analyticsEntity.getcIP());
				objmaxMindData.setDate(analyticsEntity.getDate());
				objmaxMindData.setTime(analyticsEntity.getTime());
				String csURIStem = analyticsEntity.getCsUriStem();
				objmaxMindData.setWowzaServer(csURIStem);
				objmaxMindData.setServerIp(csURIStem.substring(0,csURIStem.indexOf(':')+5));
				objmaxMindData.setxApp(analyticsEntity.getxApp());
            	objmaxMindData = maxMindService.getISPData(objmaxMindData); 
            	maxMindService.saveLogData(objmaxMindData);
           }
        	
           analyticsEntityService.saveLogData(analyticsEntity);
           makeNetworkStatsEntry();
        }
    }
    
    private void makeNetworkStatsEntry(){
    	try{
    	long currTime = System.currentTimeMillis();
    	URL urlConn =new URL("http://localhost:8086/connectioncounts");
        HttpURLConnection httpConnection= (HttpURLConnection) urlConn.openConnection();
        if(httpConnection.getResponseCode()==200)
        {
       try {
                DocumentBuilder builder= DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = builder.parse(httpConnection.getInputStream());
                doc.getDocumentElement().normalize();
                NodeList list = doc.getElementsByTagName("TimeRunning");
                if(list.item(2).getParentNode().toString().contains("ApplicationInstance"))
                {
                	System.out.println(list.item(2).getTextContent());
                }
                NodeList bytesOut = doc.getElementsByTagName("MessagesOutBytesRate");
                NodeList bytesIn = doc.getElementsByTagName("MessagesInBytesRate");
                NodeList currCon = doc.getElementsByTagName("ConnectionsCurrent");
                NodeList timeRunning = doc.getElementsByTagName("TimeRunning");
                NodeList streamName = doc.getElementsByTagName("Name");
                NetworkStats networkStats = new NetworkStats();
                networkStats.setDate(new Date(currTime));
                networkStats.setTime(new Time(currTime));
                networkStats.setBytesIn(bytesIn.item(3).getTextContent());
                networkStats.setBytesOut(bytesOut.item(3).getTextContent());
                networkStats.setConnectionCount(currCon.item(3).getTextContent());
                networkStats.setTimeRunning(Double.parseDouble(timeRunning.item(2).getTextContent()));
                networkStats.setStreamName(streamName.item(3).getTextContent());
                networksStatsService.saveNetworkStats(networkStats);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("...............Completed Netrowk entry....................");
    }catch(Exception e)
    {
    	e.printStackTrace();
    }
    	
   }


}

package com.nexteon.logging.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: AnalyticsEntity
 *
 */
@Entity
@Table(name="MaxMindData")
public class MaxMindData implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6793700536834860094L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String ip;
	private Date date;
	private Time time;
	private String country;
	private String state;
	private String city;
	private String isp;
	private String wowzaServer;
	private String serverIp;
	private String xApp;
	private String countryISOCode;
	private String stateISOCode;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the time
	 */
	public Time getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(Time time) {
		this.time = time;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the isp
	 */
	public String getIsp() {
		return isp;
	}
	/**
	 * @param isp the isp to set
	 */
	public void setIsp(String isp) {
		this.isp = isp;
	}
	/**
	 * @return the wowzaServer
	 */
	public String getWowzaServer() {
		return wowzaServer;
	}
	/**
	 * @param wowzaServer the wowzaServer to set
	 */
	public void setWowzaServer(String wowzaServer) {
		this.wowzaServer = wowzaServer;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the serverIp
	 */
	public String getServerIp() {
		return serverIp;
	}
	/**
	 * @param serverIp the serverIp to set
	 */
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}
	/**
	 * @return the xApp
	 */
	public String getxApp() {
		return xApp;
	}
	/**
	 * @param xApp the xApp to set
	 */
	public void setxApp(String xApp) {
		this.xApp = xApp;
	}
	/**
	 * @return the countryISOCode
	 */
	public String getCountryISOCode() {
		return countryISOCode;
	}
	/**
	 * @param countryISOCode the countryISOCode to set
	 */
	public void setCountryISOCode(String countryISOCode) {
		this.countryISOCode = countryISOCode;
	}
	/**
	 * @return the stateISOCode
	 */
	public String getStateISOCode() {
		return stateISOCode;
	}
	/**
	 * @param stateISOCode the stateISOCode to set
	 */
	public void setStateISOCode(String stateISOCode) {
		this.stateISOCode = stateISOCode;
	}
	
	

	
	
	
}

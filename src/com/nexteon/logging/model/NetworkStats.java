package com.nexteon.logging.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * Created by Vikas Chauhan on 23-May-17.
 */
@Entity
@Table(name="NetworkStats")
public class NetworkStats implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4782555678809235492L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private Date date;
    private Time time;
    private String bytesIn;
    private String bytesOut;
    private String streamName;
    private Double timeRunning;
    private String connectionCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getBytesIn() {
        return bytesIn;
    }

    public void setBytesIn(String bytesIn) {
        this.bytesIn = bytesIn;
    }

    public String getBytesOut() {
        return bytesOut;
    }

    public void setBytesOut(String bytesOut) {
        this.bytesOut = bytesOut;
    }

    public String getConnectionCount() {
        return connectionCount;
    }

    public void setConnectionCount(String connectionCount) {
        this.connectionCount = connectionCount;
    }

	public String getStreamName() {
		return streamName;
	}

	public void setStreamName(String streamName) {
		this.streamName = streamName;
	}

	public Double getTimeRunning() {
		return timeRunning;
	}

	public void setTimeRunning(Double timeRunning) {
		this.timeRunning = timeRunning;
	}
    
    
}

package com.nexteon.logging.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: AnalyticsEntity
 *
 */
@Entity
@Table(name="Analytics")
public class AnalyticsEntity implements Serializable {
	
	private static final long serialVersionUID = 4000110010L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date date;
	private Time time;
	private String tz;
	private String xEvent;
	private String xCategory;
	private String xSeverity;
	private String xStatus;
	private String xCtx;
	private String xComment;
	private String xVhost;
	private String xApp;
	private String xAppinst;
	private String xDuration;
	private String sIP;
	private String sPort;
	private String sUri;
	private String cIP;
	private String cProto;
	private String cReferrer;
	private String cUserAgent;
	private String cClientID;
	private String csBytes;
	private String scBytes;
	private String xStreamID;
	private String xSpos;
	private String csStreamBytes;
	private String scStreamBytes;
	private String xSname;
	private String xSnameQuery;
	private String xFileName;
	private String xFileExt;
	private String xFileSize;
	private String xFileLength;
	private String xSuri;
	private String xSuriStem;
	private String xSuriQuery;
	private String csUriStem;
	private String csUriQuery;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public String getTz() {
		return tz;
	}
	public void setTz(String tz) {
		this.tz = tz;
	}
	public String getxEvent() {
		return xEvent;
	}
	public void setxEvent(String xEvent) {
		this.xEvent = xEvent;
	}
	public String getxCategory() {
		return xCategory;
	}
	public void setxCategory(String xCategory) {
		this.xCategory = xCategory;
	}
	public String getxSeverity() {
		return xSeverity;
	}
	public void setxSeverity(String xSeverity) {
		this.xSeverity = xSeverity;
	}
	public String getxStatus() {
		return xStatus;
	}
	public void setxStatus(String xStatus) {
		this.xStatus = xStatus;
	}
	public String getxCtx() {
		return xCtx;
	}
	public void setxCtx(String xCtx) {
		this.xCtx = xCtx;
	}
	public String getxComment() {
		return xComment;
	}
	public void setxComment(String xComment) {
		this.xComment = xComment;
	}
	public String getxVhost() {
		return xVhost;
	}
	public void setxVhost(String xVhost) {
		this.xVhost = xVhost;
	}
	public String getxApp() {
		return xApp;
	}
	public void setxApp(String xApp) {
		this.xApp = xApp;
	}
	public String getxAppinst() {
		return xAppinst;
	}
	public void setxAppinst(String xAppinst) {
		this.xAppinst = xAppinst;
	}
	public String getxDuration() {
		return xDuration;
	}
	public void setxDuration(String xDuration) {
		this.xDuration = xDuration;
	}
	public String getsIP() {
		return sIP;
	}
	public void setsIP(String sIP) {
		this.sIP = sIP;
	}
	public String getsPort() {
		return sPort;
	}
	public void setsPort(String sPort) {
		this.sPort = sPort;
	}
	public String getsUri() {
		return sUri;
	}
	public void setsUri(String sUri) {
		this.sUri = sUri;
	}
	public String getcIP() {
		return cIP;
	}
	public void setcIP(String cIP) {
		this.cIP = cIP;
	}
	public String getcProto() {
		return cProto;
	}
	public void setcProto(String cProto) {
		this.cProto = cProto;
	}
	public String getcReferrer() {
		return cReferrer;
	}
	public void setcReferrer(String cReferrer) {
		this.cReferrer = cReferrer;
	}
	public String getcUserAgent() {
		return cUserAgent;
	}
	public void setcUserAgent(String cUserAgent) {
		this.cUserAgent = cUserAgent;
	}
	public String getcClientID() {
		return cClientID;
	}
	public void setcClientID(String cClientID) {
		this.cClientID = cClientID;
	}
	public String getCsBytes() {
		return csBytes;
	}
	public void setCsBytes(String csBytes) {
		this.csBytes = csBytes;
	}
	public String getScBytes() {
		return scBytes;
	}
	public void setScBytes(String scBytes) {
		this.scBytes = scBytes;
	}
	public String getxStreamID() {
		return xStreamID;
	}
	public void setxStreamID(String xStreamID) {
		this.xStreamID = xStreamID;
	}
	public String getxSpos() {
		return xSpos;
	}
	public void setxSpos(String xSpos) {
		this.xSpos = xSpos;
	}
	public String getCsStreamBytes() {
		return csStreamBytes;
	}
	public void setCsStreamBytes(String csStreamBytes) {
		this.csStreamBytes = csStreamBytes;
	}
	public String getScStreamBytes() {
		return scStreamBytes;
	}
	public void setScStreamBytes(String scStreamBytes) {
		this.scStreamBytes = scStreamBytes;
	}
	public String getxSname() {
		return xSname;
	}
	public void setxSname(String xSname) {
		this.xSname = xSname;
	}
	public String getxSnameQuery() {
		return xSnameQuery;
	}
	public void setxSnameQuery(String xSnameQuery) {
		this.xSnameQuery = xSnameQuery;
	}
	public String getxFileName() {
		return xFileName;
	}
	public void setxFileName(String xFileName) {
		this.xFileName = xFileName;
	}
	public String getxFileExt() {
		return xFileExt;
	}
	public void setxFileExt(String xFileExt) {
		this.xFileExt = xFileExt;
	}
	public String getxFileSize() {
		return xFileSize;
	}
	public void setxFileSize(String xFileSize) {
		this.xFileSize = xFileSize;
	}
	public String getxFileLength() {
		return xFileLength;
	}
	public void setxFileLength(String xFileLength) {
		this.xFileLength = xFileLength;
	}
	public String getxSuri() {
		return xSuri;
	}
	public void setxSuri(String xSuri) {
		this.xSuri = xSuri;
	}
	public String getxSuriStem() {
		return xSuriStem;
	}
	public void setxSuriStem(String xSuriStem) {
		this.xSuriStem = xSuriStem;
	}
	public String getxSuriQuery() {
		return xSuriQuery;
	}
	public void setxSuriQuery(String xSuriQuery) {
		this.xSuriQuery = xSuriQuery;
	}
	public String getCsUriStem() {
		return csUriStem;
	}
	public void setCsUriStem(String csUriStem) {
		this.csUriStem = csUriStem;
	}
	public String getCsUriQuery() {
		return csUriQuery;
	}
	public void setCsUriQuery(String csUriQuery) {
		this.csUriQuery = csUriQuery;
	}

	public AnalyticsEntity(String ...logData){
		super();
		int i=0;
		this.date = Date.valueOf(logData[i++]);
		this.time = Time.valueOf(logData[i++]);
		this.tz = logData[i++];
		this.xEvent = logData[i++];
		this.xCategory = logData[i++];
		this.xSeverity = logData[i++];
		this.xStatus = logData[i++];
		this.xCtx = logData[i++];
		this.xComment = logData[i++];
		this.xVhost = logData[i++];
		this.xApp = logData[i++];
		this.xAppinst = logData[i++];
		this.xDuration = logData[i++];
		this.sIP = logData[i++];
		this.sPort = logData[i++];
		this.sUri = logData[i++];
		this.cIP = logData[i++];
		this.cProto = logData[i++];
		this.cReferrer = logData[i++];
		this.cUserAgent = logData[i++];
		this.cClientID = logData[i++];
		this.csBytes = logData[i++];
		this.scBytes = logData[i++];
		this.xStreamID = logData[i++];
		this.xSpos = logData[i++];
		this.csStreamBytes = logData[i++];
		this.scStreamBytes = logData[i++];
		this.xSname = logData[i++];
		this.xSnameQuery = logData[i++];
		this.xFileName = logData[i++];
		this.xFileExt = logData[i++];
		this.xFileSize = logData[i++];
		this.xFileLength = logData[i++];
		this.xSuri = logData[i++];
		this.xSuriStem = logData[i++];
		this.xSuriQuery = logData[i++];
		this.csUriStem = logData[i++];
		this.csUriQuery = logData[i];
	}
	
	public AnalyticsEntity(){}
}

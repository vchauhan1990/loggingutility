package com.nexteon.logging.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.nexteon.logging.api.NetworksStatsService;
import com.nexteon.logging.main.GetEntityManager;
import com.nexteon.logging.model.NetworkStats;


/**
 * Created by admin on 23-May-17.
 */

public class NetworkStatsServiceImpl implements NetworksStatsService {


    @Override
    public boolean saveNetworkStats(NetworkStats networkStats) {
    	boolean status = false;
		 try{
		  EntityManager entitymanager = GetEntityManager.getInstance();
	      entitymanager.getTransaction().begin();
	      entitymanager.persist( networkStats );
	      entitymanager.getTransaction( ).commit( );
	      entitymanager.close( );
	      status = true;
		 }catch(Exception e)
		 {
			 status=false;
		 }
		  return status;
    }
}

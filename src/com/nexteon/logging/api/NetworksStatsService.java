package com.nexteon.logging.api;

import com.nexteon.logging.model.NetworkStats;

/**
 * Created by Vikas Chauhan on 23-May-17.
 */
public interface NetworksStatsService {

    public boolean saveNetworkStats(NetworkStats networkStats);
}

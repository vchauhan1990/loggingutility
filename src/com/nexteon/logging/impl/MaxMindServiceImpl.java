package com.nexteon.logging.impl;

import java.io.File;
import java.net.InetAddress;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.model.IspResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.Location;
import com.maxmind.geoip2.record.Postal;
import com.maxmind.geoip2.record.Subdivision;
import com.nexteon.logging.api.MaxMindService;
import com.nexteon.logging.main.GetEntityManager;
import com.nexteon.logging.model.MaxMindData;

public class MaxMindServiceImpl implements MaxMindService {

	
/*	public  MaxMindData getEnterpriseData(String IP) {
		File database = new File("/path/to/GeoIP2-Enterprise.mmdb");

		// This creates the DatabaseReader object, which should be reused across
		// lookups.
		try (DatabaseReader reader = new DatabaseReader.Builder(database).build()) {
			InetAddress ipAddress = InetAddress.getByName(IP);

			// Use the enterprise(ip) method to do a lookup in the Enterprise
			// database
			EnterpriseResponse response = reader.enterprise(ipAddress);

			Country country = response.getCountry();
			System.out.println(country.getIsoCode()); // 'US'
			System.out.println(country.getName()); // 'United States'
			System.out.println(country.getNames().get("zh-CN")); // 
			System.out.println(country.getConfidence()); // 99

			Subdivision subdivision = response.getMostSpecificSubdivision();
			System.out.println(subdivision.getName()); // 'Minnesota'
			System.out.println(subdivision.getIsoCode()); // 'MN'
			System.out.println(subdivision.getConfidence()); // 77

			City city = response.getCity();
			System.out.println(city.getName()); // 'Minneapolis'
			System.out.println(city.getConfidence()); // 11

			Postal postal = response.getPostal();
			System.out.println(postal.getCode()); // '55455'
			System.out.println(postal.getConfidence()); // 5

			Location location = response.getLocation();
			System.out.println(location.getLatitude()); // 44.9733
			System.out.println(location.getLongitude()); // -93.2323
			System.out.println(location.getAccuracyRadius()); // 50
		} catch (Exception e) {

		}

	}*/

	@Override
	public  MaxMindData getCityData(String IP) {
		MaxMindData objMaxMindData = new MaxMindData();
		try {
			File database = new File("E:/GeoLite2-City_20170502/GeoLite2-City_20170502/GeoLite2-City.mmdb");
			objMaxMindData.setIp(IP);

			// This creates the DatabaseReader object, which should be reused
			// across
			// lookups.
			DatabaseReader reader = new DatabaseReader.Builder(database).build();

			InetAddress ipAddress = InetAddress.getByName(IP);// 128.101.101.101 US, 112.196.155.157 IN

			// Replace "city" with the appropriate method for your database,
			// e.g.,
			// "country".
			CityResponse response = reader.city(ipAddress);

			Country country = response.getCountry();
			System.out.println(country.getIsoCode()); // 'US'
			System.out.println(country.getName()); // 'United States'
			objMaxMindData.setCountry(country.getName());
			objMaxMindData.setCountryISOCode(country.getIsoCode());
		//	System.out.println(country.getNames().get("zh-CN")); // 

			Subdivision subdivision = response.getMostSpecificSubdivision();
			System.out.println(subdivision.getName()); // 'Minnesota'
			System.out.println(subdivision.getIsoCode()); // 'MN'
			objMaxMindData.setState(subdivision.getName());
			objMaxMindData.setStateISOCode(subdivision.getIsoCode());

			City city = response.getCity();
			System.out.println(city.getName()); // 'Minneapolis'
			objMaxMindData.setCity(city.getName());

			Postal postal = response.getPostal();
			System.out.println(postal.getCode()); // '55455'

			Location location = response.getLocation();
			System.out.println(location.getLatitude()); // 44.9733
			System.out.println(location.getLongitude()); // -93.2323
		} catch (Exception e) {

		}
			return objMaxMindData;
	}
	
	@Override
	public  MaxMindData getISPData(MaxMindData objmaxMindData){
		
		try{
		// A File object pointing to your GeoIP2 ISP database
		File database = new File("/path/to/GeoIP2-ISP.mmdb");

		// This creates the DatabaseReader object, which should be reused across
		// lookups.
		DatabaseReader reader = new DatabaseReader.Builder(database).build();

		InetAddress ipAddress = InetAddress.getByName(objmaxMindData.getIp());//128.101.101.101

		IspResponse response = reader.isp(ipAddress);

		System.out.println(response.getAutonomousSystemNumber());       // 217
		System.out.println(response.getAutonomousSystemOrganization()); // 'University of Minnesota'
		System.out.println(response.getIsp());                          // 'University of Minnesota'
		objmaxMindData.setIsp(response.getIsp());
		}catch(Exception e){
			
		}
		return objmaxMindData;
	}

	@Override
	public boolean saveLogData(MaxMindData maxMindData) {
		 boolean status = false;
		 try{
		  EntityManager entitymanager = GetEntityManager.getInstance();
	      entitymanager.getTransaction().begin();
	      entitymanager.persist( maxMindData );
	      entitymanager.getTransaction( ).commit( );

	      entitymanager.close( );
	      status = true;
		 }catch(Exception e)
		 {
			 status=false;
		 }
		  return status;
	}
}

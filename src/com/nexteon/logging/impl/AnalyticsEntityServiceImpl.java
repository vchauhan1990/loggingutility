package com.nexteon.logging.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.nexteon.logging.api.AnalyticsEntityService;
import com.nexteon.logging.main.GetEntityManager;
import com.nexteon.logging.model.AnalyticsEntity;

public class AnalyticsEntityServiceImpl implements AnalyticsEntityService {

	@Override
	public boolean saveLogData(AnalyticsEntity analyticsEntity) {

		 boolean status = false;
		 try{
		  EntityManager entitymanager = GetEntityManager.getInstance();
	      entitymanager.getTransaction().begin();

	      
	      entitymanager.persist( analyticsEntity );
	      entitymanager.getTransaction( ).commit( );

	      entitymanager.close( );
	      status = true;
		 }catch(Exception e)
		 {
			 status=false;
		 }
		  return status;
		
	}

	
	
}

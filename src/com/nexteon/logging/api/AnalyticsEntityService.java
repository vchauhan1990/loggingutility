package com.nexteon.logging.api;

import com.nexteon.logging.model.AnalyticsEntity;

public interface AnalyticsEntityService {

    public boolean saveLogData(AnalyticsEntity analyticsEntity);

}
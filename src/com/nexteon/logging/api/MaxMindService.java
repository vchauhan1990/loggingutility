package com.nexteon.logging.api;

import com.nexteon.logging.model.MaxMindData;

public interface MaxMindService {

    public boolean saveLogData(MaxMindData maxMindData);
    
    public  MaxMindData getCityData(String IP);
    
    public  MaxMindData getISPData(MaxMindData objmaxMindData);

}
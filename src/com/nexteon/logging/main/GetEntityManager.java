package com.nexteon.logging.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GetEntityManager {

	public static EntityManager getInstance(){
		
		  EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "LoggingUtility" );
		  EntityManager entitymanager = emfactory.createEntityManager( );
		  return entitymanager;
	}
		
}
